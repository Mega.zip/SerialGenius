#coding:utf-8

import wx
import Global

dlg=None;

def Show(content=u'Loading...'):
    global dlg;
    dlg = wx.ProgressDialog("",content,
                               maximum = 100,
                               parent=Global.var['Frame'],
                               style = wx.PD_CAN_ABORT
                                | wx.PD_APP_MODAL
                                | wx.PD_AUTO_HIDE 
                                #| wx.PD_ELAPSED_TIME
                                #| wx.PD_ESTIMATED_TIME
                                # | wx.PD_REMAINING_TIME
                                );
    dlg.Update(50)

def Hide():
    global dlg;
    if dlg:
        dlg.Destroy();
