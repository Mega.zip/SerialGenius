#coding:utf-8

import traceback;
import Config;
import datetime;


#写入日志
def Trace():
    try:
        f=open(Global.env['app_root']+'SerialGenius_Error.log','a');
        f.write(str(datetime.datetime.now())+' V'+Config.APP_CONFIG['version']+'\n')
        traceback.print_exc(file=f)  
        traceback.print_exception
        f.write("\n\n")
        f.flush()  
        f.close()
    except:
        pass;

