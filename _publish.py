#coding:utf-8
from distutils.core import setup
import os, sys
import py2exe, glob

root=os.getcwd()
if root[-1:]!=os.sep:            
    root = root + os.sep


version="1.0"
name="SerialGenius"
description="@2013 Eamonning"

manifest = ''' 
<assembly xmlns="urn:schemas-microsoft-com:asm.v1" 
manifestVersion="1.0"> 
<dependency> 
<dependentAssembly> 
<assemblyIdentity 
type="win32" 
name="Microsoft.VC90.CRT" 
version="9.0.21022.8" 
processorArchitecture="x86" 
publicKeyToken="1fc8b3b9a1e18e3b" 
/> 
</dependentAssembly> 
</dependency> 
<dependency> 
<dependentAssembly> 
<assemblyIdentity 
type="win32" 
name="Microsoft.Windows.Common-Controls" 
version="6.0.0.0" 
processorArchitecture="x86" 
publicKeyToken="6595b64144ccf1df" 
language="*" 
/> 
</dependentAssembly> 
</dependency> 
</assembly> 
''' 


windows=[{
            "script":"SerialGenius.py",
            "icon_resources": [(1, "dll/ico.dll")],
            "other_resources":[(24,1,manifest)],  
        }]


#includes=[];

options={}
options["py2exe"]={
    "dist_dir":"SerialGenius",
    "compressed": True, #True,
    "optimize": 2, #2, #0,  
    'bundle_files': 3,#3,#1,
    'includes':[
                'matplotlib.backends',
                'matplotlib.figure',
                'pylab',
                'numpy',
                'matplotlib.backends.backend_wxagg',
                'module.*','mpl_toolkits',
                'matplotlib.backends.backend_tkagg',
                'matplotlib.*',
                ],
    'excludes':['_gtkagg','_agg2','_cairo','_cocoaagg',
                  '_fltkagg','_gtk','_gtkcairo',
                  'pydoc','doctest','test'],
    'dll_excludes':['libgdk-win32-2.0-0.dll','libgobject-2.0-0.dll',],
}

data_files=[
        (r'dll', glob.glob(root+'dll\*.*')),
        (r'mpl-data', glob.glob(r'C:\Python27\Lib\site-packages\matplotlib\mpl-data\*.*')),
        (r'mpl-data', [r'C:\Python27\Lib\site-packages\matplotlib\mpl-data\matplotlibrc']),
        (r'mpl-data\images', glob.glob(r'C:\Python27\Lib\site-packages\matplotlib\mpl-data\images\*.*')),
        (r'mpl-data\fonts', glob.glob(r'C:\Python27\Lib\site-packages\matplotlib\mpl-data\fonts\*.*')),
        
    ];

setup(
    version=version,
    description=description,
    name=name,
    author='Eamonning',
    author_email='i@eamonning.com',
    url='http://www.eamonning.com',
    data_files=data_files,
    windows=windows,
    zipfile='dll/dlls.lib',#None,#'dll/dlls.lib',
    options=options
)
