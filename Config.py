#coding:utf-8

import SerialUtil
import Global

#程序配置
APP_CONFIG={
    'name'            : Global.app['name'],
    'description'     : Global.app['description'],
    'version'         : Global.app['version'],
    'copyright'       : Global.app['copyright'],
    'setting':{
        'default_baudrate': '9600',
        'default_bytesize': '8',
        'default_parity'  : 'None',
        'default_stopbits': '1',
        'default_port'    : 'None',
        'show_modules'    : '[0][1][2][3]',
    },
    'module_recv':{
        'hexMode'         : '0',
    },
    'module_send':{
        'hexMode'         : '0',
        'sendNewLine'     : '0',
        'sendInterval'    : '0',
        'sendChars'       : '',
        'sendHexs'        : '',
    },
    'module_terminal':{
        'sendNewLine'     : '0',
        'charset'         : '0',        
    },
    'module_variable':{
        'hexMode'         : '0',
    },
    'module_variable_track':{
        
    },
    'module_video':{
        'saveJpg':  '0',
    },
}
