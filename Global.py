#coding:utf-8

app={
    'name'            : 'SerialGenius',
    'description'     : u'你一定会爱上的串口助手 波峰电子定制版',
    'version'         : '1.3',
    'copyright'       : 'Copyright (c) www.eamonning.com',
    'email'           : 'i@eamonning.com',
    'author'          : 'Eamonn',
    'version_check'   : 'http://www.eamonning.com/project/checkversion/7',
    'download_url'    : 'http://www.eamonning.com/project/view/7',
    'suggestion_url'  : 'http://www.eamonning.com/project/view/7',
}

env={
  'screen':{'width':400,'height':300},
  'accelerator_Key':[],
  'app_root':'',
  'data_root':'Data/',
  'show_full_screen':False,
}

# Global Variable
#   Usually the window handler
var={
    # Basic layout
    'Frame':None,
    'PanelTool':None,
    'PanelMain':None,
}

#打开的串口
SerialGeniusPort=None

#模块
Modules=   ['recv',      
            'send',      
            'terminal',   
            'variable',
            'variabletrack',
            'video',
            'rehoodetector'
            ];
ModulesName=[u'接收数据', 
             u'发送数据', 
             u'超级终端', 
             u'变量助手', 
             u'变量跟踪', 
             u'二维图像',
             u'金属探测'
            ];
         